const prevNext = require('../src/utils/prev-next');
const dateSort = require('../src/utils/date-sort');
const siteConfig = require('../data/SiteConfig');
const draft = require('../src/utils/draft');
const assert = require('assert');

const given = [
  {
    fields: {
      date: '2018-01-01',
      title: 'Item 1',
    },
  },
  {
    fields: {
      title: 'Draft 1',
    },
  },
  {
    fields: {
      date: '2018-01-03',
      title: 'Item 2',
    },
  },
  {
    fields: {
      date: '2018-02-01',
      title: 'Item 3',
    },
  },
  {
    fields: {
      title: 'Draft 2',
    },
  },
  {
    fields: {
      title: 'Draft 3',
    },
  },
  {
    fields: {
      date: '2018-02-04',
      title: 'Item 5',
    },
  },
  {
    fields: {
      date: '2018-02-02',
      title: 'Item 4',
    },
  },
];

// console.info('\n-----------------------------------------\n\n\n');

assert.strictEqual(draft(given[0]), false);
assert.strictEqual(draft(given[1]), true);

const sorted = given.sort(dateSort(siteConfig.dateFromFormat));

let result;

result = prevNext(sorted, 0);
// console.info(
//   0,
//   (result.prev || { fields: {} }).fields.title,
//   sorted[0].fields.title,
//   (result.next || { fields: {} }).fields.title,
// );
assert.strictEqual(result.next.fields.title, 'Item 2');


result = prevNext(sorted, 2);
// console.info(
//   2,
//   (result.prev || { fields: {} }).fields.title,
//   sorted[2].fields.title,
//   (result.next || { fields: {} }).fields.title,
// );
assert.strictEqual(result.prev.fields.title, 'Item 1');
assert.strictEqual(result.next.fields.title, 'Item 3');

result = prevNext(sorted, 7);
// console.info(
//   7,
//   (result.prev || { fields: {} }).fields.title,
//   sorted[7].fields.title,
//   (result.next || { fields: {} }).fields.title,
// );
assert.strictEqual(result.prev.fields.title, 'Item 4');
