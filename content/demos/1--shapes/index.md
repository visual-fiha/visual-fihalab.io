---
title: Drawing shapes
description: Drawing basic shapes with Visual Fiha
gist: df56dd88b283807950ab854f155674ab
cover: shapes.png
---

## Setup script

`gist:zeropaper/df56dd88b283807950ab854f155674ab?file=layer__canvas__setup.js`


## Animation script

`gist:zeropaper/df56dd88b283807950ab854f155674ab?file=layer__canvas__animation.js`