---
title: Fiha!
description: First steps with Visual Fiha
gist: 1fd6d1757b6a96edb0db8c6b619eba84
cover: fiha.png
---

What would be a coding demonstration without a simple ~~"Hello World!"~~ "Fiha!"?

The most versatile way to draw text with Fiha is probably the
`textLines()` function on a _canvas_ layer.

`gist:zeropaper/1fd6d1757b6a96edb0db8c6b619eba84?file=layer__canvas__animation.js`