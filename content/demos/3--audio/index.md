---
title: Audio
description: Make visuals react to sound
gist: 2ec72a8007e6f9da46df73b0026eec85
cover: audio.png
---

## Animation script

`gist:zeropaper/2ec72a8007e6f9da46df73b0026eec85?file=layer__audio__animation.js`
