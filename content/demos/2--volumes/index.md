---
title: Drawing volumes
description: Drawing 3d volumes with Visual Fiha
gist: 85fafd38796c3785aa5601db5d1d6433
cover: volumes.png
---

## Setup script

`gist:zeropaper/85fafd38796c3785aa5601db5d1d6433?file=layer__three__setup.js`


## Animation script

`gist:zeropaper/85fafd38796c3785aa5601db5d1d6433?file=layer__three__animation.js`
