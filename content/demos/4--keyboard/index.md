---
title: Keyboard
description: Use the keyboard to change settings
gist: 6e4559229a65924066349155cc7904ee
cover: keyboard.png
---

## Setup script

`gist:zeropaper/6e4559229a65924066349155cc7904ee?file=layer__base__setup.js`


## Animation script

`gist:zeropaper/6e4559229a65924066349155cc7904ee?file=layer__base__animation.js`
