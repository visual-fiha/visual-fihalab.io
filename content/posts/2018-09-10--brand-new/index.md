---
title: It's a brand new version
category: "blog"
cover: controller-layers.png
author: Valentin Vago
tags:
  - release
---

The first intention of building a VJing software that runs in browsers
dates back to [2013][1] as the W3C started discussing WebMIDI
implementation.
The development of the first version of Visual Fiha begun in
October 2016.
That first version was based on the amazing [AmpersandJS][2] library,
it supported live manipulation of [THREE.js][3] scenes, HTML5 canvas,
SVG, HTML with CSS, integrated the [P5][4] library as well and
implemented the concept of signals.

Visual Fiha was first used for a live performance in December 2016 and
a first workshop took place in May 2017 in Moscow.

It is in autumn 2017 that the decision was taken to attempt a complete
rewrite and re-design with [React.js][5] and [Redux][6].

The new version is hosted on GitLab because of the integrated CI/CD
tools it offers.

After almost a year of free time development, there is still numerous
bugs which need to be addressed but its state advanced enough give the
users a good impression of what it should become.

[1]: https://github.com/zeropaper/visual-fiha/commit/3dd8297e8856d86f1db24b4f349d707f34569742
[2]: https://ampersandjs.com/
[3]: http://threejs.org
[4]: https://p5js.org/
[5]: https://reactjs.org/
[6]: https://redux.js.org/