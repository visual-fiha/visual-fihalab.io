---
title: Scripting
category: documentation
tags:
  - concept
  - general
---

Scripting is the key to use Visual Fiha. You can already have some fun fairly intuitively if you have some JavaScript experience. However, it is critical to understand the following concepts in order to create smooth, fast, complex and clean code.

## Setup

This script is run once when it changes or its layer initialises.
Its role is to prepare the layer for animation.

For instance, it is in the setup script that you should load assets (and then pass them using the "cache" object).
It is also the place to put code which will not be modified "live" during a performance.

## Animation

Run every time the browser can allow it, the modifications made in a animation script impact almost instantly rendered result.

## The cache object

You can use the special "cache" object in order to pass variables (of all kind) between a setup script and an animation script or between frames in a animation script.

The cache object is scoped to its layer or the worker but there is an way to pass data between the worker and layers.

Here is what could be placed in a setup script:

```js
cache.drawRedDot = (x, y) => circle({
  x,
  y,
  radius: vmin(1),
  fill: 'red',
});
```

and here is how the animation script can use the variables defined in the script above:

```js
cache.drawRedDot(width(2), height(2));
```

You might find convenient to keep track of values between frames in a animation script.
In the following script, the canvas will be cleared at each first frame of a beat.

```js
const beatPrct = read('beat');
if (cache.beatPrct > beatPrct) clear();
cache.beatPrct = beatPrct;

circle({
  x: width() * beatPrct,
  y: height(2),
  stroke: 'white'
});
```
You can see the result [here][1].

## Layers

More information about the layers [here][2].

## Worker

You can run scripts on a worker in order to share values between layers or to do some heavy computation.
Using the worker animation script allows you to guarantee synchronisation between layers.
The worker setup and animation scripts should return an object of simple values (objects, arrays, strings, numbers).

### Passing data from worker to layers

Let's imagine that we want some values to be computed for animation on several layers.

```js
const frq = read('frequencies', []);
const frqAvg = arrayAvg(frq);
```

Instead of placing the above code in all layer animation scripts, you should add it in the animation script of the worker as follow.

```js
const frq = read('frequencies', []);

const data = {
  frqAvg: arrayAvg(frq),
};

return data;
```

And then in each layer animation scripts use:

```js
const frqAvg = read('arrayAvg', 0);
```

This is significantly faster to compute and produces cleaner code.

## Reference

You can quickly find some documentation related to the context (layer type or worker) by clicking on the question mark (?) at the top of the editor.


[1]: https://visual-fiha.gitlab.io/controller/#gid=62caaf6455ea9d24d89611805b80d576
[2]: ./layer-types/
