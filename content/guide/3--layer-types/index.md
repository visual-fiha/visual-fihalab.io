---
title: Layer Types
category: documentation
tags:
  - concept
  - general
---

Visual Fiha implements 3 different types of layer to serve different goals.

## Canvas

The canvas layer is meant for 2d animation and its API is based on the DOM Canvas API but is tweaked
for live coding convenience.
The scope of the layer script is more or less a 2d canvas element context.

Original DOM Canvas API:

```js
const context = document.getElementById('canvas').getContext('2d');

context.fillStyle = 'lime';
context.fillRect(0, 0, context.canvas.width, context.canvas.height);
```

Would be written as follow in the Visual Fiha canvas layer script:
```js
fillStyle('lime');
fillRect(0, 0, width(), height());
```

## Three.js

This layer type is meant to animate 3d scenes. In the scripts (setup and animation) have the following globals defined:

- `camera`: is an instance of a [`THREE.PrespectiveCamera`][1]
- `scene`: is an instance of a [`THREE.Scene`][2]
- `renderer`: is an instance of a [`THREE.Renderer`][3]

**Note:** in order to have something rendered, you need to call the "renderer.render" method
with the "scene" and "camera" at the end of the animation script of the layer like that:

```js
renderer.render(scene, camera);
```

Three.js has a complete [reference][4] and countless of [examples][5].

## Paper.js

Implements the [paper.js][6] library.

[1]: https://threejs.org/docs/#api/en/cameras/PerspectiveCamera
[2]: https://threejs.org/docs/#api/en/scenes/Scene
[3]: https://threejs.org/docs/#api/en/constants/Renderer
[4]: https://threejs.org/docs/
[5]: https://threejs.org/examples/
[6]: http://paperjs.org/