---
title: Architecture
category: documentation
tags:
  - concept
  - advanced
draft: true
---

[![Architecture diagram][1]][1]

## Controller

The collect of "signals" (audio, MIDI, keyboard and mouse) is done by the controller
and sent to the worker (using the worker `postMessage` function).

## Worker

The worker role is to collect and treat signal data from the controller, the setup persistence (in browser or gist) and web sockets.

It is possible (and advised for performance and timing) to run [setup and animation scripts][2] to process data which will be sent to the displays and controller (using the broadcast channel).

## Displays

Like the worker, layers of a display have [setup and animation scripts][2]. The display renders by executing the animation sctipt reading a data buffer object.
The data buffer object is updated independently (using the broadcast channel) from the rendering.


[1]: ./fiha-architecture-original.svg
[2]: /guide/scripting/
