---
title: About
---

The Visual Fiha web app is a personal and non-commercial project from Valentin Vago (zeropaper) aimed for him to experiment with web related technologies.

The first intention of building a VJing software that runs in browsers
dates back to [2013][1] as the W3C started discussing WebMIDI
implementation.
The development of the first version of Visual Fiha begun in
October 2016.
That first version was based on the amazing [AmpersandJS][2] library,
it supported live manipulation of [THREE.js][3] scenes, HTML5 canvas,
SVG, HTML with CSS, integrated the [P5][4] library as well and
implemented the concept of signals.

Visual Fiha was first used for a live performance in December 2016 and
a first workshop took place in May 2017 in Moscow.

It is in autumn 2017 that the decision was taken to attempt a complete
rewrite and re-design with [React.js][5] and [Redux][6].


## Terms of use

Usage of Visual Fiha is at your own risk.

## Privacy Policy

Basically Visual Fiha and its website do not store nor use information to track you in any ways.
The app saves runtime (like your configuration or setups) information in your browser.<br />
However some third party services used to serve Visual Fiha (like Internet Service providers or GitLab who hosts the app and its website) could.


[1]: https://github.com/zeropaper/visual-fiha/commit/3dd8297e8856d86f1db24b4f349d707f34569742
[2]: https://ampersandjs.com/
[3]: http://threejs.org
[4]: https://p5js.org/
[5]: https://reactjs.org/
[6]: https://redux.js.org/
