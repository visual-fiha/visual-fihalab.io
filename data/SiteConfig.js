module.exports = {
  siteTitle: 'Visual Fiha', // Site title.
  siteTitleShort: 'Visual Fiha', // Short site title for homescreen (PWA). Preferably should be under 12 characters to prevent truncation.
  siteTitleAlt: 'Visual Fiha', // Alternative site title for SEO.
  siteLogo: '/logos/icon-512.png', // Logo used for SEO and manifest.
  siteUrl: 'https://visual-fiha.gitlab.io', // Domain of your website without pathPrefix.
  appUrl: process.env.FIHA_PATH,
  pathPrefix: '/', // Prefixes all links. For cases when deployed to example.github.io/gatsby-advanced-starter/.
  siteDescription:
    'Web based live coding VJing app with audio processing and MIDI control support.', // Website description used for RSS feeds/meta description tag.
  siteRss: '/rss.xml', // Path to the RSS file.
  siteFBAppID: '', // FB Application ID for using app insights
  googleAnalyticsID: '', // GA tracking ID.
  disqusShortname: '', // Disqus shortname.
  postDefaultCategoryID: 'Tech', // Default category for posts.
  dateFromFormat: 'YYYY-MM-DD', // Date format used in the frontmatter.
  dateFormat: 'YYYY-MM-DD', // Date format for display.
  userName: '', // Username to display in the author segment.
  userTwitter: '', // Optionally renders "Follow Me" in the UserInfo segment.
  userLocation: '', // User location to display in the author segment.
  userAvatar: '', // User avatar to display in the author segment.
  userDescription: '', // User description to display in the author segment.
  // Links to social profiles/projects you want to display in the author segment/navigation bar.
  userLinks: [
    // {
    //   label: "GitHub",
    //   url: "https://github.com/Vagr9K/gatsby-advanced-starter",
    //   iconClassName: "fa fa-github"
    // },
    // {
    //   label: "Twitter",
    //   url: "https://twitter.com/Vagr9K",
    //   iconClassName: "fa fa-twitter"
    // },
    // {
    //   label: "Email",
    //   url: "mailto:vagr9k@gmail.com",
    //   iconClassName: "fa fa-envelope"
    // }
  ],
  copyright: '', // Copyright string for the footer of the website and RSS feed.
  themeColor: 'hsl(261, 100%, 75%)', // Used for setting manifest and progress theme colors.
  backgroundColor: 'hsl(70, 8%, 15%)', // Used for setting manifest background color.
  theme: {
    fontFamily: 'monospace',
    spacing: {
      xs: '0.6rem',
      s: '0.8rem',
      m: '1rem',
      l: '1.5rem',
      xl: '1.75rem',
    },
    breakpoints: {
      tablet: 425,
      desktop: 1024,
      largeDesktop: 1280,
    },
    header: {
      height: {
        default: '3rem',
      },
    },
    color: {
      neutral: {
        black: 'hsl(0, 0%, 0%)',
        black2: 'hsl(60, 17%, 11%)',
        black3: 'hsl(70, 8%, 15%)',
        blue: 'hsl(190, 81%, 67%)',
        grey: 'hsl(55, 8%, 26%)',
        orange: 'hsl(32, 98%, 56%)',
        orange2: 'hsl(30, 83%, 34%)',
        orange3: 'hsl(47, 100%, 79%)',
        purple: 'hsl(261, 100%, 75%)',
        red: 'hsl(0, 93%, 59%)',
        red2: 'hsl(338, 95%, 56%)',
        white: 'hsl(0, 0%, 97%)',
        white2: 'hsl(60, 36%, 96%)',
        white3: 'hsl(60, 30%, 96%)',
        yellow: 'hsl(54, 70%, 68%)',
        yellow2: 'hsl(80, 76%, 53%)',
        yellow3: 'hsl(60, 12%, 79%)',
        yellow4: 'hsl(55, 11%, 22%)',
        yellow5: 'hsl(50, 11%, 41%)',
        toolbarbg: 'hsl(210, 7%, 94%)',
        sidebarbg: 'hsl(210, 11%, 93%)',
        sidebarhighlightbg: 'hsl(216, 10%, 80%)',
      },
    },
  },
};
