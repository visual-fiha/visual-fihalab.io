const { theme } = require('./data/SiteConfig');

const breakpointsPixel = {};
Object.keys(theme.breakpoints).forEach(name => {
  breakpointsPixel[name] = `${theme.breakpoints[name]}px`;
});

// console.info('postcss-easy-media-query', breakpointsPixel);

module.exports = () => ({
  plugins: {
    'postcss-easy-media-query': {
      breakpoints: breakpointsPixel,
    },
    'postcss-text-remove-gap': {
      defaultFontFamily: 'Open Sans',
      defaultLineHeight: '0',
    },
    'postcss-nested': {},
    'postcss-preset-env': {},
  },
});
