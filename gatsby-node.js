const path = require('path');
const _ = require('lodash');
const siteConfig = require('./data/SiteConfig');
const dateSort = require('./src/utils/date-sort');
const draft = require('./src/utils/draft');
const prevNext = require('./src/utils/prev-next');

const { NO_DRAFTS, NODE_ENV } = process.env;

const nodesByType = {
  post: [],
  page: [],
  // demo: [],
  guide: [],
  api: [],
};

function addSiblingNodes(createNodeField) {
  Object.keys(nodesByType).forEach(type => {
    const nodes = nodesByType[type];

    if (type === 'post') {
      nodes.sort(dateSort(siteConfig.dateFromFormat));
    } else {
      nodes.sort((a, b) => {
        if (a.fileAbsolutePath > b.fileAbsolutePath) return 1;
        if (a.fileAbsolutePath < b.fileAbsolutePath) return -1;
        return 0;
      });
    }

    const count = nodes.length;
    nodes.forEach((node, i) => {
      const { prev, next } = prevNext(nodes, i);

      createNodeField({
        node: node,
        name: 'isFirst',
        value: i === 0,
      });
      createNodeField({
        node: node,
        name: 'isLast',
        value: i === count - 1,
      });

      if (next) {
        createNodeField({
          node: node,
          name: 'nextTitle',
          value: next.frontmatter.title,
        });
        createNodeField({
          node: node,
          name: 'nextSlug',
          value: next.fields.slug,
        });
      }

      if (prev) {
        createNodeField({
          node: node,
          name: 'prevTitle',
          value: prev.frontmatter.title,
        });
        createNodeField({
          node: node,
          name: 'prevSlug',
          value: prev.fields.slug,
        });
      }
    });
  });
}

const dateExp = /([0-9]{4,4}-[0-1][0-9]-[0-3][0-9])(--([^/]*))/i;
const indexExp = /([0-9]*)(--([^/]*))/i;

const getMarkdownContext = edge => {
  const {
    frontmatter: { category, tags, title },
    fields: {
      slug,
      type,
      date,
      index,
      prevTitle,
      nextTitle,
      editURL,
      prevSlug,
      nextSlug,
    },
  } = edge.node;

  return {
    title,
    date,
    index,
    slug,
    type,
    tags,
    category,
    editURL,
    prevTitle,
    nextTitle,
    prevSlug,
    nextSlug,
  };
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  if (node.internal.type !== 'MarkdownRemark') return;

  const { createNodeField } = actions;
  const fileNode = getNode(node.parent);
  const { name, dir } = path.parse(fileNode.relativePath);

  const matchDate = (dir || '').match(dateExp);
  const matchIndex = (dir || '').match(indexExp);

  let slug;
  let pathDate;

  if (matchDate) {
    pathDate = matchDate[1]; // eslint-disable-line
    slug = `/${matchDate[3]}/`;

    createNodeField({
      node,
      name: 'date',
      value: pathDate,
    });
  } else if (matchIndex) {
    slug = `/${matchIndex[3]}/`;

    createNodeField({
      node,
      name: 'index',
      value: parseInt(matchIndex[1], 10),
    });
  } else {
    createNodeField({
      node,
      name: 'index',
      value: null,
    });
  }

  if (!slug) {
    if (name === 'README') {
      slug = `/${dir}`;
    } else if (name !== 'index' && dir !== '') {
      slug = `/${dir}/${name}/`;
    } else if (dir === '') {
      slug = `/${name}/`;
    } else {
      slug = `/${dir}/`;
    }
  }

  let pageType = 'page';
  if (node.fileAbsolutePath.includes('content/post')) {
    pageType = 'post';
    slug = `/post${pathDate ? '/' + pathDate : ''}${slug}`;
  } else if (node.fileAbsolutePath.includes('content/demo')) {
    pageType = 'demo';
    slug = `/demo${slug}`;
  } else if (node.fileAbsolutePath.includes('content/guide')) {
    pageType = 'guide';
    slug = `/guide${slug}`;
  } else if (node.fileAbsolutePath.includes('src')) {
    pageType = 'api';
    slug = `/api${slug}`;
  }

  let editURL = null;
  if (pageType === 'api') {
    if (!node.frontmatter) return;
    editURL = `https://gitlab.com/zeropaper/fiha/-/edit/master/src/${dir}/README.md`;
  }

  console.info(' - %s\t%s', pageType, slug);

  createNodeField({ node, name: 'editURL', value: editURL });
  createNodeField({ node, name: 'slug', value: slug });
  createNodeField({ node, name: 'type', value: pageType });
  nodesByType[pageType].push(node);
};

exports.setFieldsOnGraphQLNodeType = ({ type, actions }) => {
  const { name } = type;
  const { createNodeField } = actions;
  if (name === 'MarkdownRemark') {
    addSiblingNodes(createNodeField);
  }
};

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  return new Promise((resolve, reject) => {
    const postPage = path.resolve('src/templates/post.jsx');
    const apiPage = path.resolve('src/templates/api.jsx');
    const tagPage = path.resolve('src/templates/tag.jsx');
    const categoryPage = path.resolve('src/templates/category.jsx');

    graphql(`
      {
        allMarkdownRemark {
          edges {
            node {
              frontmatter {
                tags
                category
                title
              }
              fields {
                type
                slug
                editURL
                type
                date
                index
                prevSlug
                nextSlug
                prevTitle
                nextTitle
              }
            }
          }
        }
      }
    `).then(result => {
      if (result.errors) {
        // eslint-disable-next-line no-console
        console.log(result.errors);
        return reject(result.errors);
      }

      const tagSet = new Set();
      const categorySet = new Set();
      console.info('\nPages:');
      result.data.allMarkdownRemark.edges.filter(edge => !draft(edge.node)).forEach(edge => {
        if (edge.node.fields.type === 'demo') return;

        if (edge.node.fields.type === 'api') {
          if (!edge.node.frontmatter.title) return;
        }

        if (edge.node.frontmatter.tags) {
          edge.node.frontmatter.tags.forEach(tag => {
            tagSet.add(tag);
          });
        }

        if (edge.node.frontmatter.category) {
          categorySet.add(edge.node.frontmatter.category);
        }

        console.info(' - %s\t%s', edge.node.fields.type, edge.node.frontmatter.title);

        createPage({
          path: edge.node.fields.slug,
          component: edge.node.fields.type === 'api' ? apiPage : postPage,
          context: getMarkdownContext(edge),
        });
      });

      const tagList = Array.from(tagSet);
      tagList.forEach(tag => {
        createPage({
          path: `/tag/${_.kebabCase(tag)}/`,
          component: tagPage,
          context: {
            slug: `/tag/${_.kebabCase(tag)}`,
            tag,
          },
        });
      });

      const categoryList = Array.from(categorySet);
      categoryList.forEach(category => {
        createPage({
          path: `/category/${_.kebabCase(category)}/`,
          component: categoryPage,
          context: {
            slug: `/category/${_.kebabCase(category)}`,
            category,
          },
        });
      });

      resolve();
    });
  });
};

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type MarkdownRemarkFields implements Node {
      isFirst: Boolean
      editURL: String
      prevSlug: String
      prevTitle: String
      nextSlug: String
      nextTitle: String
      isLast: Boolean
    }
  `
  createTypes(typeDefs)
};
