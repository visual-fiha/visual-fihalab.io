import React from 'react';
import Helmet from 'react-helmet';
import 'normalize.css';

import config from '../../data/SiteConfig';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

const IndexLayout = ({ children }) => (
  <React.Fragment>
    <Helmet>
      <meta name="description" content={config.siteDescription} />
    </Helmet>
    <Header config={config} />
    {children}
    <Footer config={config} />

    <style jsx global>{`
      *,
      *:after,
      *:after {
        box-sizing: border-box;
      }

      :root {
        line-height: 1.6;
        font-size: 13px;

        @from-width tablet {
          font-size: 15px;
        }

        @above tablet {
          font-size: 16px;
        }
      }

      body {
        font-family: ${config.theme.fontFamily};
        background: ${config.theme.color.neutral.white3};
        color: ${config.theme.color.neutral.black};
      }

      a {
        &,
        &:link,
        &:hover,
        &:focus,
        &:active,
        &:visited {
          text-decoration: none;
          color: ${config.theme.color.neutral.orange};
          cursor: pointer;
        }

        &:hover,
        &:focus {
          color: ${config.theme.color.neutral.orange2};
        }
      }

      p,
      ul,
      ol,
      dl,
      blockquote {
        margin-top: 0;
        margin-bottom: ${config.theme.spacing.m};
      }

      li > p:last-of-type {
        margin-bottom: 0;
      }
    `}</style>
  </React.Fragment>
);

export default IndexLayout;
