import React from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';

import Layout from '../layout';
import PostListing from '../components/PostListing/PostListing';
import SEO from '../components/SEO/SEO';
import config from '../../data/SiteConfig';
import Hero from '../components/Hero/Hero';

const Index = props => (
  <Layout>
    <Helmet title={config.siteTitle} />
    <SEO />
    <Hero theme={config.theme} />
    <PostListing postEdges={props.data.posts.edges} />
  </Layout>
);

export default Index;

export const pageQuery = graphql`
  query IndexQuery {
    posts: allSitePage(
      limit: 15
      filter: { context: { type: { eq: "post" } } }
      sort: { fields: [context___date], order: DESC }
    ) {
      totalCount
      edges {
        node {
          path
          context {
            title
            slug
            tags
            category
            type
            date
          }
        }
      }
    }
  }
`;
