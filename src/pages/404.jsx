import React from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';

import Layout from '../layout';
// import PostListing from '../components/PostListing/PostListing';
// import SEO from '../components/SEO/SEO';
import config from '../../data/SiteConfig';
// import Hero from '../components/Hero/Hero';

const Index = props => (
  <Layout>
    <Helmet title={config.siteTitle} />

    <h1>404 - Not Found</h1>
  </Layout>
);

export default Index;
