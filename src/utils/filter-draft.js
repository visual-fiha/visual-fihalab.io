const { NODE_ENV } = process.env;
const dev = NODE_ENV !== 'production';

module.exports = {
  edges: (edges = []) =>
    edges.filter(
      edge =>
        dev ||
        ((edge.node.context || edge.node.fields).date === null &&
          (edge.node.context || edge.node.fields).index === null),
    ),
  nodes: (nodes = []) =>
    nodes.filter(
      node =>
        dev ||
        ((node.context || node.field).date === null && (node.context || node.field).index === null),
    ),
};
