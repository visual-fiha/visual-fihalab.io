const { NODE_ENV, NO_DRAFTS } = process.env;
const exlcudeDrafts = !!NO_DRAFTS || NODE_ENV === 'production';

module.exports = ({ fields: { date, index, type } }) => type !== 'api' &&
  (exlcudeDrafts &&
    (typeof date === 'undefined' || date === null || date === '') &&
    (typeof index === 'undefined' || index === null || index === ''));
