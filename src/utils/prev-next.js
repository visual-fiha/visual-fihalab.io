const draft = require('./draft');

module.exports = (array, index) => {
  let prev = null;
  let next = null;

  for (let prevIndex = index - 1; prevIndex >= 0 && !prev; prevIndex -= 1) {
    if (!prev && array[prevIndex] && !draft(array[prevIndex])) prev = array[prevIndex];
  }

  for (let nextIndex = index + 1; nextIndex < array.length && !next; nextIndex += 1) {
    if (!next && array[nextIndex] && !draft(array[nextIndex])) next = array[nextIndex];
  }

  return {
    prev,
    next,
  };
};
