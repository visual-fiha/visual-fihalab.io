const moment = require('moment');

module.exports = dateFromFormat => ({ fields: { date: date1 } }, { fields: { date: date2 } }) => {
  const dateA = moment(date1, dateFromFormat);
  const dateB = moment(date2, dateFromFormat);

  if (dateA.isBefore(dateB)) return -1;
  if (dateB.isBefore(dateA)) return 1;
  return 0;
};
