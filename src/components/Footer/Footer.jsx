import React from 'react';

const Footer = ({ config }) => (
  <React.Fragment>
    <footer className="footer">
      {/* Based on{' '}
      <a href="https://github.com/Vagr9K/gatsby-advanced-starter">Gatsby Advanced Starter</a>. */}
    </footer>

    <style jsx>{`
      footer {
        padding: ${config.theme.spacing.s} ${config.theme.spacing.m};
      }
    `}</style>
  </React.Fragment>
);

export default Footer;
