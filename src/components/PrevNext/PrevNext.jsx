import React from 'react';
import { Link } from 'gatsby';

const PrevNext = ({ prevSlug, prevTitle, nextSlug, nextTitle }) => (
  <React.Fragment>
    <div className="prev-next">
      {prevSlug && prevTitle ? (
        <div className="prev">
          <Link to={prevSlug}>{prevTitle}</Link>
        </div>
      ) : null}
      {nextSlug && nextTitle ? (
        <div className="next">
          <Link to={nextSlug}>{nextTitle}</Link>
        </div>
      ) : null}
    </div>

    <style jsx>{`
      .prev-next {
        display: flex;

        > div {
          flex-grow: 1;

          &::before,
          &::after {
            display: inline-block;
          }
        }
      }

      .prev {
        text-align: left;

        &::before {
          content: '«';
          margin-right: 1rem;
        }
      }

      .next {
        text-align: right;

        &::after {
          content: '»';
          margin-left: 1rem;
        }
      }
    `}</style>
  </React.Fragment>
);

export default PrevNext;
