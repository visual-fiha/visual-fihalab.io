import React from 'react';
import { Link } from 'gatsby';

import { theme } from '../../../data/SiteConfig';

class PostListing extends React.Component {
  get postList() {
    const postList = [];
    this.props.postEdges.forEach(({ node }) => {
      const context = node.context || {};
      postList.push({
        path: context.slug,
        tags: context.tags,
        cover: context.cover,
        title: context.title,
        date: context.date,
        excerpt: node.excerpt,
        timeToRead: node.timeToRead,
      });
    });
    return postList;
  }

  render() {
    return (
      <React.Fragment>
        <div className="posts">
          {this.postList.map(post => (
            <Link to={post.path} key={post.path}>
              <h1>{post.title}</h1>
            </Link>
          ))}
        </div>

        <style jsx>{`
          .posts {
            padding: ${theme.spacing.m};
          }
        `}</style>
      </React.Fragment>
    );
  }
}

export default PostListing;
