export default {
  audio: {
    name: 'Audio',
    description: `With Visual Fiha you can make your visuals react to sound. The browser usually
    use the microphone as audio source but you can set it to use an other input
    like a USB port.`,
    position: {
      top: '65%',
      left: '15%',
    },
  },
  'live-coding': {
    name: 'Live Coding',
    description: `The changes made in the editor can be applied instantly or on demand. The
    editor supports multiple selections, error handling tools and the usual coder
    must-haves.`,
    position: {
      top: '36%',
      left: '55%',
    },
  },
  bpm: {
    name: 'BPM',
    description: `Tap the button or type the amount in the input and to have visuals on the
    beat.`,
    position: {
      top: '71%',
      left: '34.5%',
    },
  },
  layers: {
    name: 'Layers',
    description: `Setups are like ogres. They have layers.
    In Visual Fiha layers are not all the same though and the following types are available:
    Three.js for 3D
    DOM Canvas with a twist for 2D
    paper.js (to demonstrate that the system is exstensible).`,
    position: {
      top: '56%',
      left: '1%',
    },
  },
  midi: {
    name: 'MIDI Control',
    description: `Like, seriously, you can control your visuals (made in a browser) with a Kaos Pad 3!
    For instance.
    A few other controllers are available out-of-the-box but if you have the device, you can fairly easily extend Visual Fiha.`,
    position: {
      bottom: '52%',
      left: '5%',
    },
  },
  reader: {
    name: 'Reader',
    description: 'Helps you to keep an eye on the value of your variables.',
    position: {
      bottom: '45%',
      left: '12%',
    },
  },
  storage: {
    name: 'Storage',
    description: `You can use the storage of your browser or save your setups as Gists and share them.`,
    position: {
      top: '10%',
      left: '1%',
    },
  },
  reference: {
    name: 'API Reference',
    description: `Code easy and learning fast with a contextual reference and library of snippets.`,
    position: {
      bottom: '15%',
      right: '2%',
    },
  },
};
