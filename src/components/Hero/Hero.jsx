import React from 'react';
import { graphql, StaticQuery } from 'gatsby';

import AppFeatures from './AppFeatures';

import bgkPattern from './bgk-pattern.png';

class Hero extends React.Component {
  componentDidMount() {
    this.featuresInterval = setInterval(this.animateFeature, 10000);
  }

  componentWillUnmount() {
    clearInterval(this.featuresInterval);
  }

  featureIndex = 0;
  features = [];
  animateFeature = () => {
    this.features.forEach((el, e) => {
      el.classList[this.featureIndex === e ? 'add' : 'remove']('focused');
    });
    if (this.featureIndex + 1 === this.features.length) {
      this.featureIndex = 0;
    } else {
      this.featureIndex += 1;
    }
  };

  refFeature = el => {
    if (!el) {
      this.features = [];
      return;
    }

    // el.parentNode.style.visibility = 'hidden';
    const txt = el.querySelector('.description');
    txt.parentNode.style.transition = 'none';
    txt.parentNode.style.minWidth = '200px';
    el.classList.add('focused');

    txt.style.width = `${txt.clientWidth}px`;
    txt.style.height = `${txt.clientHeight}px`;

    el.classList.remove('focused');
    txt.parentNode.removeAttribute('style');
    // el.parentNode.style.visibility = 'visible';

    this.features.push(el);
  };

  renderData = ({ file }) => {
    const { props } = this;
    const { theme } = props;

    const images = {
      mobile: file.mobile.fixed,
      tablet: file.tablet.fixed,
      desktop: file.desktop.fixed,
      largeDesktop: file.largeDesktop.fixed,
    };

    return (
      <React.Fragment>
        <section className="hero">
          <div className="picture-wrapper">
            <div className="features-wrapper">
              <picture>
                <source srcSet={images.mobile.srcSet} media="(max-width: 767px)" />

                <source
                  srcSet={images.tablet.srcSet}
                  media="(min-width: 768px) and (max-width: 1023px)"
                />

                <source
                  srcSet={images.desktop.srcSet}
                  media="(min-width: 1024px) and (max-width: 1279px)"
                />

                <source srcSet={images.largeDesktop.srcSet} media="(min-width: 1280px)" />

                <img src={images.mobile.src} alt="screenshot of Visual Fiha controller" />
              </picture>

              <ul className="features">
                {Object.keys(AppFeatures).map(key => (
                  <li key={key} ref={this.refFeature} style={AppFeatures[key].position}>
                    <a href={`#${key}`}>{AppFeatures[key].name}</a>
                    <div>
                      <div className="description">{AppFeatures[key].description}</div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>

          <div className="button-wrapper">
            <a href={`${process.env.FIHA_PATH}controller`} target="controller">
              <span className="main">Give it a try</span>
              <span className="small">Works best with Google Chrome</span>
            </a>
          </div>
        </section>

        {/* --- STYLES --- */}
        <style jsx>{`
          .hero {
            display: flex;
            flex-flow: column nowrap;
            justify-content: space-evenly;
            padding-top: ${theme.header.height.default};
            height: 100vh;
            overflow: hidden;

            @below desktop {
              height: auto;
            }

            @below tablet {
              padding-top: 0;
            }
          }

          .picture-wrapper,
          .button-wrapper {
            padding: 3rem;
            text-align: center;
            overflow: visible;

            @below tablet {
              padding: 0;
            }
          }

          .picture-wrapper {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            position: relative;
            background: ${theme.color.neutral.black3} url(${bgkPattern});

            picture,
            source,
            img {
              margin: auto;
              display: block;
              max-width: 100%;
              max-height: 100%;
              overflow: hidden;
            }

            .features-wrapper {
              position: relative;

              @below tablet {
                position: static;
                margin: 1rem;
              }
            }

            .features {
              margin: 0;
              padding: 0;
              position: absolute;
              width: 100%;
              height: 100%;
              top: 0;
              left: 0;
              list-style: none;

              @below tablet {
                position: static;
              }

              > li {
                display: flex;
                flex-direction: column;
                position: absolute;
                background: ${theme.color.neutral.blue};
                padding: 0.1em 0.3em;
                border-radius: 0;
                text-align: left;
                max-width: 200px;

                background: ${theme.color.neutral.blue};
                z-index: 1;

                @below tablet {
                  position: static;
                  max-width: none;
                  margin: 1rem 0;
                }

                > a,
                > div {
                  position: relative;
                  color: ${theme.color.neutral.black};
                }

                > a {
                  z-index: 2;
                  font-weight: 700;
                }

                > div {
                  z-index: 1;
                  font-size: 0.8rem;
                  overflow: hidden;
                  position: relative;
                  max-width: 0;
                  max-height: 0;
                  transition: max-height 309ms ease, max-width 309ms ease 309ms;

                  > div {
                    overflow: hidden;
                    position: relative;
                  }

                  @below tablet {
                    max-width: none;
                    max-height: none;
                  }
                }

                &:hover,
                &:focus,
                &.focused {
                  z-index: 2;

                  > div {
                    max-width: 200px;
                    max-height: 500px;

                    @below tablet {
                      max-width: none;
                      max-height: none;
                    }
                  }
                }

                &:focus,
                &:hover {
                  z-index: 3;
                }

                &:after {
                  border: solid transparent;
                  content: ' ';
                  height: 0;
                  width: 0;
                  position: absolute;
                  pointer-events: none;
                }
              }
            }
          }

          .button-wrapper {
            padding: 3rem 3rem;

            a {
              text-align: center;
              display: inline-block;
              margin: auto;
              padding: 0.3em 0.7em;
              background-color: ${theme.color.neutral.purple};
              color: ${theme.color.neutral.white};

              > :global(span) {
                display: block;
              }

              > :global(.main) {
                font-weight: 700;
                font-size: 2rem;
              }

              > :global(.small) {
                font-size: 0.75rem;
              }
            }
          }
        `}</style>
      </React.Fragment>
    );
  };

  render() {
    return (
      <StaticQuery
        query={graphql`
          query {
            file(relativePath: { regex: "/controller-audio/" }) {
              mobile: childImageSharp {
                fixed(width: 450, height: 253, quality: 90) {
                  srcSet
                  src
                }
              }
              tablet: childImageSharp {
                fixed(width: 700, height: 394, quality: 90) {
                  srcSet
                  src
                }
              }
              desktop: childImageSharp {
                fixed(width: 1000, height: 562, quality: 90) {
                  srcSet
                  src
                }
              }
              largeDesktop: childImageSharp {
                fixed(width: 1200, height: 675, quality: 90) {
                  srcSet
                  src
                }
              }
            }
          }
        `}
        render={this.renderData}
      />
    );
  }
}

export default Hero;
