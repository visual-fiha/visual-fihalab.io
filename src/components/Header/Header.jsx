import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';

import GLIcon from './gitlab-logo.svg';

export const fihaIcon = ({ orange }) => `<svg
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   version="1.1"
   viewBox="0 0 1000 1000"
   height="1000"
   width="1000">
  <path style="stroke:${orange};stroke-width:50;fill:none;stroke-linecap:round;stroke-linejoin:round;" d="m 42.144772,100.70423 460.738378,798.02231 137.92084,-238.11891 -100.83949,0 -83.42113,-144.48961 267.68175,0 78.38889,-135.77355 -424.45952,0 -83.42215,-144.49139 591.30382,0 78.02825,-135.14885 z" />
</svg>`;

export const HeaderComponent = ({ guide = {}, pages = {}, api = {}, theme, siteDescription, siteTitle }) => (
  <React.Fragment>
    <header>
      <div className="inner">
        <h1>
          <Link to="/" title="Home">
            <div
              className="fiha-logo"
              dangerouslySetInnerHTML={{ __html: fihaIcon(theme.color.neutral) }}
            />
            {siteTitle}
          </Link>
        </h1>

        <div className="description">{siteDescription}</div>

        <ul>
          <li className="has-children">
            <span className="link">Guide</span>

            <ul>
              {(guide.edges || []).map(({ node: { context: { title, slug } } }) => (
                <li key={slug}>
                  <Link to={slug}>{title}</Link>
                </li>
              ))}
            </ul>
          </li>

          <li className="has-children">
            <span className="link">API</span>

            <ul>
              {(api.edges || []).map(({ node: { context: { title, slug } } }) => (
                <li key={slug}>
                  <Link to={slug}>{title || slug || 'WTF???'}</Link>
                </li>
              ))}
            </ul>
          </li>

          {(pages.edges || []).map(({ node: { context: { title, slug } } }) => (
            <li key={slug}>
              <Link to={slug}>{title}</Link>
            </li>
          ))}

          <li>
            <a
              target="_blank"
              rel="noopener noreferrer"
              title="Source code on GitLab"
              className="icon icon-gitlab"
              href="https://gitlab.com/zeropaper/fiha"
            >
              <span>Source</span>
            </a>
          </li>
        </ul>
      </div>
    </header>

    <style jsx>{`
      header {
        display: flex;
        flex-direction: column;
        justify-content: center;
        padding: 0 ${theme.spacing.m};
        background-color: ${theme.color.neutral.white3};
        position: relative;
        z-index: 100;
        border: 1px solid ${theme.color.neutral.orange};
        border-width: 0 0 1px 0;

        @above tablet {
          position: fixed;
          left: 0;
          right: 0;
          top: 0;
          height: ${theme.header.height.default};

          > .inner {
            display: flex;
            height: 100%;
            align-items: baseline;
          }
        }
      }

      h1 {
        margin: 0;
        font-size: 1.2rem;
      }

      .fiha-logo {
        display: inline-block;
        margin-right: ${theme.spacing.m};

        > :global(svg) {
          height: 1.2rem;
          width: 1.2rem;
          transform: scale(1.7) translateY(0.1rem) translateX(0.2rem);
        }
      }

      ul {
        list-style: none;
      }

      .inner > ul {
        flex-grow: 1;
        display: flex;
        flex-wrap: wrap;
        margin: 0;
        padding: 0;
        justify-content: flex-end;
        height: 100%;
      }

      .description {
        font-size: 0.8rem;
        margin-left: ${theme.spacing.l};

        @below largeDesktop {
          display: none;
        }
      }

      .inner > ul > li {
        display: flex;
        flex-direction: column;
        justify-content: center;
        padding-left: ${theme.spacing.s};
        padding-right: ${theme.spacing.s};

        &:last-of-type {
          margin-right: -${theme.spacing.s};
        }
      }

      .has-children {
        position: relative;

        > span {
          color: ${theme.color.neutral.orange};
          cursor: pointer;
        }

        > ul {
          display: none;
          white-space: nowrap;
          max-width: 50vw;
          position: absolute;
          right: -1px;
          top: 100%;
          padding: 0;
          background: ${theme.color.neutral.sidebarbg};
          border: 1px solid ${theme.color.neutral.orange};
          border-width: 0 1px 1px 1px;

          > li :global(a) {
            display: block;
            padding: ${theme.spacing.s};
          }
        }

        &:hover,
        &.show-children {
          background: ${theme.color.neutral.sidebarbg};

          > span {
            color: ${theme.color.neutral.orange2};
          }

          > ul {
            display: block;
            text-align: right;
          }

          @above tablet {
            > ul {
              display: block;
            }
          }

          :global(a) {
            &:hover,
            &:focus {
              background: ${theme.color.neutral.sidebarhighlightbg};
            }
          }
        }
      }

      .icon {
        width: 1em;
        height: 1em;

        &-gitlab {
          background: url(${GLIcon}) no-repeat center;
          background-size: contain;
        }

        span {
          display: none;
        }
      }
    `}</style>
  </React.Fragment>
);

const Header = props => {
  const {
    config: { siteTitle, theme, siteDescription },
  } = props;
  return (
    <StaticQuery
      render={data => (
        <HeaderComponent
          {...data}
          siteTitle={siteTitle}
          theme={theme}
          siteDescription={siteDescription}
        />
      )}
      query={graphql`
        query {
          pages: allSitePage(limit: 8, filter: { context: { type: { eq: "page" } } }) {
            edges {
              node {
                context {
                  slug
                  title
                  index
                }
              }
            }
          }
          guide: allSitePage(filter: { context: { type: { eq: "guide" } } }) {
            edges {
              node {
                context {
                  slug
                  title
                  index
                }
              }
            }
          }
          api: allSitePage(filter: { context: { type: { eq: "api" } } }) {
            edges {
              node {
                context {
                  slug
                  title
                }
              }
            }
          }
        }
      `}
    />
  );
};

export default Header;
