import React from 'react';
import _ from 'lodash';
import { Link } from 'gatsby';

const PostTags = ({ tags, config }) => (
  <React.Fragment>
    <ul className="post-tag-container">
      {tags &&
        tags.map(tag => (
          <li key={tag}>
            <Link to={`/tags/${_.kebabCase(tag)}`}>{tag}</Link>
          </li>
        ))}
    </ul>

    <style jsx>{`
      .post-tag-container {
        list-style: none;
        padding: 0;
        margin: 0;

        li {
          display: inline-block;
          margin-right: ${config.theme.spacing.m};
        }
      }
    `}</style>
  </React.Fragment>
);

export default PostTags;
