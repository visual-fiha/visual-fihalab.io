import React from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import Layout from '../layout';
import PrevNext from '../components/PrevNext/PrevNext';
import SEO from '../components/SEO/SEO';
import config from '../../data/SiteConfig';

const PostTemplate = props => {
  const { slug } = props.pageContext;
  const {
    markdown,
    page: {
      context: { prevSlug, prevTitle, nextSlug, nextTitle, date, index, title },
    },
  } = props.data;

  const {
    theme: {
      color,
      spacing,
      header: { height },
    },
  } = config;

  return (
    <Layout>
      <Helmet>
        <title>{`${title} | ${config.siteTitle}`}</title>
      </Helmet>

      <SEO postPath={slug} postNode={{ ...markdown, title, date }} postSEO />

      <article className={date === null && index === null ? 'draft' : ''}>
        <header>
          <h1>{title}</h1>
          <PrevNext {...{ prevSlug, prevTitle, nextSlug, nextTitle }} />
        </header>

        <div role="main" dangerouslySetInnerHTML={{ __html: markdown.html }} />

        <footer>
          {/* <PostTags tags={post.tags} config={config} /> */}
          <PrevNext {...{ prevSlug, prevTitle, nextSlug, nextTitle }} />
        </footer>
      </article>

      <style jsx>{`
        article {
          margin: auto;

          @above tablet {
            max-width: calc(700px + (4 * ${spacing.xl}));
            padding-top: ${height.default};
          }
        }

        header,
        [role='main'],
        footer {
          padding: ${spacing.s} ${spacing.m};
        }

        header {
          h1 {
            margin: 0;
          }
        }

        @above tablet {
          header,
          [role='main'] > :global(*:not(.gatsby-highlight)),
          footer {
            margin-left: calc(2 * ${spacing.xl});
            margin-right: calc(2 * ${spacing.xl});
          }
        }

        [role='main'] {
          > :global(.gatsby-highlight),
          > :global(p > .gatsby-resp-image-link) {
            background-color: ${color.neutral.black3};
            color: ${color.neutral.white};
            margin-bottom: ${spacing.m};
            overflow: auto;
            padding: 0 ${spacing.m};

            @above tablet {
              padding: 0 calc(2 * ${spacing.xl});
            }
          }

          > :global(.gatsby-highlight .keyword),
          > :global(.gatsby-highlight .function) {
            color: ${color.neutral.blue};
          }
          > :global(.gatsby-highlight .operator),
          > :global(.gatsby-highlight .template-string .punctuation) {
            color: ${color.neutral.red};
          }
          > :global(.gatsby-highlight .string) {
            color: ${color.neutral.yellow};
          }
          > :global(.gatsby-highlight .function-variable) {
            color: ${color.neutral.yellow2};
          }

          > :global(p > img),
          > :global(p > a > img) {
            max-width: 100%;
          }

          > :global(p > .gatsby-resp-image-link) {
            margin-left: -${spacing.m};
            margin-right: -${spacing.m};
            padding: 0;

            @above tablet {
              margin-left: calc(-2 * ${spacing.xl});
              margin-right: calc(-2 * ${spacing.xl});
            }
          }
        }
      `}</style>
    </Layout>
  );
};

export default PostTemplate;

export const pageQuery = graphql`
  query PostTemplate($slug: String!) {
    page: sitePage(context: { slug: { eq: $slug } }) {
      context {
        index
        date
        prevTitle
        prevSlug
        nextTitle
        nextSlug
        title
        category
        tags
      }
    }
    markdown: markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      timeToRead
      excerpt
    }
  }
`;
